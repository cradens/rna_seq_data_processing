#!/usr/bin/env bash

# Given a list of bam files and a GTF, use HTSEQ to count reads in genes

bamfiles=${1} # list of bam file paths
gtf_file=${2} # /data/DB/hg19/ensembl.hg19.gtf
ended=${3} # paired or single
stranded=${4} # reverse_stranded, forward_stranded, non_stranded
metafeature=${5} #exon
feature=${6} #gene_id
threads=${7} #number of threads 1+ 
outdirectory=${8} # path to directory where the counts should be saved

# make sure bamfiles is set
if [ -z ${bamfiles} ]
then
        echo "bamfiles is unset .. please specify file with list of bams (arg 1)"
        exit 1
fi

# make sure gtf_file is set
if [ -z ${gtf_file} ]
then
        echo "gtf_file is unset .. please specify gtf (e.g. /data/DB/hg19/ensembl.hg19.gtf) to use... (arg 2)"
        exit 1
fi

if [ ! -f ${gtf_file} ]
then
        echo "${gtf_file} isn't recognized as a valid file..."
        exit 1
fi

# make sure ended is set
if [ -z ${ended} ]
then
        echo "ended is unset .. please specify whether data are single or paired... (arg 3)"
        exit 1
fi

if [[ ! ${ended} = "single" ]]
then
	if [[ ! ${ended} = "paired" ]]
	then
		echo "ended needs to be 'paired' or 'single', not '${ended}' ..."
		exit 1
	else
		paired=" -p"
	fi
else
	paired=""
fi

# make sure stranded is set
if [ -z ${stranded} ]
then
        echo "stranded is unset .. please specify whether data are reverse_stranded, forward_stranded, or non_stranded... (arg 4)"
        exit 1
fi

if [[ ! ${stranded} = "reverse_stranded" ]]
then
	if [[ ! ${stranded} = "forward_stranded" ]]
	then
		if [[ ! ${stranded} = "non_stranded" ]]
		then
			echo "stranded needs to be 'reverse_stranded' or 'forward_stranded', or 'non_stranded', not '${stranded}' ... (arg 4)"
			exit 1
		else
			strand=""
		fi
	else
		strand="-s 1"
	fi
else
	strand=" -s 2"
fi

# make sure metafeature is set
if [ -z ${metafeature} ]
then
        echo "metafeature is unset .. please specify what featureCounts should call a metafeature (e.g. 'exon') (arg 5)"
        exit 1
fi

if [[ ! ${metafeature} = "exon" ]]
then
	echo "Warning! metafeature isn't set to 'exon' .. it is set to ${metafeature} ... do you know what you're doing???"
fi

# make sure feature is set
if [ -z ${feature} ]
then
        echo "feature is unset .. please specify what featureCounts should call a feature (e.g. 'gene_id') (arg 6)"
        exit 1
fi

if [[ ! ${feature} = "gene_id" ]]
then
	echo "Warning! feature isn't set to 'gene_id' .. it is set to ${feature} ... do you know what you're doing???"
fi

# make sure threads is set
if [ -z ${threads} ]
then
	echo "threads is unset .. please specify what featureCounts should call a threads (arg 7)"
	exit 1
fi

# make sure threads is an integer
if [[ ! $threads =~ ^-?[0-9]+$ ]]
then
	echo "threads needs to be an integer (arg 7)"
fi

# make sure outdirectory is set
if [ -z ${outdirectory} ]
then
        echo "outdirectory is unset .. please specify a directory to save counts to (arg 8)"
        exit 1
fi

if [ ! -d ${outdirectory} ]
then
        echo "${outdirectory} isn't recognized as a valid directory..."
        exit 1
fi

# Check dependencies are installed
command -v featureCounts >/dev/null 2>&1 || { echo >&2 "I require featureCounts but it's not installed.  Aborting."; exit 1; }

wcres=($(wc ${bamfiles}))
n_samps=${wcres[0]} # 1st elem from wc is the # of lines...

echo "Getting featureCounts runlines for ${n_samps} bam files ..."

cd ${outdirectory}

if [ ! -f ${bamfiles} ]
then
        echo "${bamfiles} isn't recognized as a valid file (is full path spelled out?)..."
	cd -
        exit 1
fi

if [ -f featureCount_runlines.sh ]
then
	echo "Warning! featureCount_runlines.sh exists in ${outdirectory} ... are you sure you want to overwrite the runlines? Please delete the runlines file, or change the name..."
	exit 1
fi

while read -r bam
do
	# escapes weird things in the name like spaces and parantheses
	bam=$(printf %q "$bam")
	
	# check that bam path points to actual bam
	if [ ! -f ${bam} ]
	then
		echo "${bam} doesn't exist... check your bam file list (are the paths correct?) ..."
		if [ -f featureCount_runlines.sh ]
		then
			rm featureCount_runlines.sh
		fi
		cd -
		exit 1
	fi
	s=${bam##*/}
	srr=${s%.*}
	#echo "Counting reads for $srr ..."
	echo "featureCounts ${bam}${paired}${strand} -t ${metafeature} -g ${feature} -T ${threads} -a ${gtf_file} -o ${PWD}/${srr}_raw_counts.txt" >> featureCount_runlines.sh
	#htseq-count $bam ${gtf_file} > ${srr}_raw_counts.txt --format bam
done < ${bamfiles}

echo "# bash gene_counts.sh ${1} ${2} ${3} ${4} ${5} ${6} ${7} ${8}" > TEMPORARYFILE
cat featureCount_runlines.sh >> TEMPORARYFILE
cat TEMPORARYFILE > featureCount_runlines.sh
rm TEMPORARYFILE

cd -

echo "done! Bash script to calculate gene counts (named 'featureCount_runlines.sh') located here: ${outdirectory}"
