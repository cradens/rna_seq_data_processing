#!/usr/bin/env bash

# Caleb Matthew Radens

feature_counts_res=${1} # filepath to file with list of filepaths (outputs from featureCounts)

# make sure feature_counts_res is set
if [ -z ${feature_counts_res} ]
then
        echo "feature_counts_res is unset .. please specify directory with featureCounts outputs (arg 1)"
        exit 1
fi

# make sure feature_counts_res is set
if [ ! -f ${feature_counts_res} ]
then
        echo "${feature_counts_res} is not a valid filepath .. please specify file with list of featureCounts output files (arg 1)"
        exit 1
fi

echo "feature_counts_res is ${feature_counts_res} and pattern_match is ${pattern_match}"

current_dir=${pwd}
results_dir=$(dirname ${feature_counts_res})
cd ${results_dir}

# Each featureCounts output has a pre-header that is skipped, saved to a new temp file
while read -r rcfile
do
	printf "skipping header and saving temp file from:\n${rcfile}\n"
	newname=${rcfile//raw_counts/raw_counts_skip1}
	tail -n +2 ${rcfile} | cat > ${newname}
done < ${feature_counts_res}

nlines=$(wc -l ${feature_counts_res} | awk '{print $1}')
printf "Merging columns from ${nlines} featureCounts results files. This may take a while..."
# adapted from https://stackoverflow.com/a/14598945/7378802
# this takes the 7th column (read counts) from all *raw_counts_skip1.txt files and merges them
awk '{ a[FNR] = (a[FNR] ? a[FNR] FS : "") $7 } END { for(i=1;i<=FNR;i++) print a[i] }' ./*raw_counts_skip1.txt > colmerged_counts.txt

# Use last newname defined above to extract the gene names
echo "Getting gene IDs from ${newname} ..."
awk '{print $1}' ${newname} > gene_ids_list_temp.txt
paste -d' ' gene_ids_list_temp.txt colmerged_counts.txt > TEMPORARY.txt

cat TEMPORARY.txt > colmerged_counts.txt

rm ./*raw_counts_skip1.txt
rm gene_ids_list_temp.txt
rm TEMPORARY.txt

cd ${current_dir}

echo "Done!"
