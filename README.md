# rna_seq_data_processing

This is a collection of wrapper scripts to make RNA_Seq data processing faster/easier.

## Check Strandedness of bam file:
bash check_strandednes.sh <bam file here.bam> <'chr' if bams chromosome names have chr, otherwise blank>

This script checks for reads from the GAPDH gene region of mouse and human.
Checks if the reads are forward, reverse, mate1, or mate2.

## Download sra -> trim -> fastqc given SRRs or ERRs
bash download_fastqs <filepath with list of SRR/ERRs> <# to download at once> <single/paired> <filepath to save download lines to>

NOTE: this script depends on fastq-dump, trim_galore, and fastqc

## Simply download sras
bash download_sras.sh <filepath with list of SRR/ERRs> <# to download at once> <filepath to save download lines to>

IF on HPC/LPC, first log in to networked computer (i.e. ssh username@mercury.pmacs.upenn.edu)

## Align fastqs with STAR
bash align_fastqs.sh <filepath with list of fastqs> <mm10/hg19> <single/paired> <read length> <# of threads> <# characters at end of filenames> <filepath to save alignment run lines to>

NOTES:

 - depends on STAR and samtools
 
 - Explanation of <# characters at end of filenames> :
 
Use this to indicate how many characters at the end of each sample are indicating the filetype and/or pair ID. For example, if you have donor_A_pair1.fq and donor_A_pair2.fq, use 9

 - Example:
 
How to use this script for paired data;
a) save a file with a line-by-line list of fastqs. Each two lines correspond to a pair of fastqs
      donor_A_1.fq
      donor_A_2.fq
      donor_B_1.fq
      donor_B_2.fq
      etc
b) If you data are from mouse, read length 100, run the following line:
bash /data/STAR_DATA/align_fastqs.sh fastq_file_list.txt mm10 paired 100 8 5 /path/to/runlines/here.txt

## Calculate gene counts from bams using featureCounts
bash gene_counts.sh <filepath to list of bamfiles> <filepath to gtf file> <single/paired> <reverse_stranded/forward_stranded/non_stranded>  <metafeature (i.e. exon)> <feature (i.e. gene_id)> <# threads> <directory to save counts to>

NOTE: depends on featureCounts


## Python script to generate run lines for HPC (bjob-compatible)
python HPC_sra_to_bam_paired_end.py
python HPC_sra_to_bam_single_end.py

There are 5 steps in these scripts. For each step, there is a 'skip=True' line. Change to skip=False
 if you want to generate runlines for that step. I assume you have a folder named after each sample in 
 the sra_list. (download_sras.sh will do this - downloads each sra to its own folder). Runlines for
 each sample will be saved in the sample folders. A master wrapper called 'submit_<step here>.sh' will
 be saved in the the project directory. The master wrapper has the bjob submission lines to run the
 sample runlines. Simply run 'bash submit_<step here>.sh' to fire off bjob submissions for all the samples.

Step 1: Obtain sras (see download_sras.sh, for e.g.)
Step 2: Obtain fastqs (dump sras to fastq; if you already have fastqs, skip to Step 3)
Step 3: Trim and fastQC
Step 4: STAR-align
Step 5: Salmon to obtain effective gene counts and effective gene lengths

First, copy the appropriate paired/single-end python script to your project directory
Then, modify the following variables where appropriate:

### project-specific parameters
threads = 8 # default 8 should be sufficient
project_dir='/project/klynclab/tuomela_th17_2016/' # CHANGE
sra_list=os.path.join(project_dir,'sras.txt') # CHANGE- this can be either SRRs, ERRs, or sample names
bamdir = os.path.join(project_dir,'bams') # DO NOT CHANGE
transcriptome_bams_dir = os.path.join(project_dir,'transcriptome_bams') # DO NOT CHANGE
star_genome='/project/klynclab/GENOMES/human/hg38_GRCh38_Reference_Genome/star_genomes/hg38_len100_fromGTF/' # CHANGE
salmon_genome_dir='/project/klynclab/GENOMES/human/hg38_GRCh38_Reference_Genome/salmon_genomes/hg38_salmon_transcriptome.fa' # CHANGE
salmon_library_type='U' #nonstranded, single end # CHANGE
### Software (non-local)
moduleloadline='module load java/openjdk-1.8.0 python/3.6.3 STAR/2.5.2a FastQC-0.11.2' # DO NOT CHANGE
trimgalore='/project/klynclab/software/TrimGalore-0.5.0/trim_galore' # CHANGE
samtools='/project/klynclab/software/samtools-1.9/samtools' # CHANGE
fastqdump='/project/klynclab/software/sratoolkit.2.9.2-centos_linux64/bin/fastq-dump' # CHANGE
### Software (local)
salmon='/home/cradens/bin/salmon-0.11.3-linux_x86_64/bin/salmon' # CHANGE
NOTE: Salmon (salmon-0.11.3-linux_x86_6) is not compatible with hg19-derived GFF/GTFs.

