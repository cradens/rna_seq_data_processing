#!/usr/bin/env bash

srr_list=${1} # path/name to/of file with list of SRRs or ERRs
n_process_at_once=${2} # how many fastqs to download and process at once? Recommended: 6
downloadlinepath=${3} # where to save the download lines?

# make sure sample_list is set
if [ -z ${srr_list} ]
then
	echo "srr_list is unset .. please specify file with list of SRRs (arg 1)"
	exit 1
fi

if [ ! -f ${srr_list} ]
then
	echo "${srr_list} is not a valid file path to a list of SRRs..."
	exit 1
fi

# make sure n_process_at_once is set
if [ -z ${n_process_at_once} ]
then
	echo "n_process_at_once is unset .. please number of SRRs to process at once (arg 2)"
	exit 1
fi

# make sure downloadlinepath is set
if [ -z ${downloadlinepath} ]
then
	echo "downloadlinepath is unset .. please say where you want the download lines saved to (arg 4)"
	exit 1
fi

if [ -f ${downloadlinepath} ]
then
	echo "${downloadlinepath} already exists, please delete, move, or change name ..."
	exit 1
fi

if [ -d ${downloadlinepath} ]
then
        echo "${downloadlinepath} is a directory, please give a path and name for a file to save the lines to..."
        exit 1
fi

#first_srr=$( head -1 ${srr_list} )
#first_srr_6char=${first_srr:0: 6}
#echo "downloading SRRs with the following prefix: ${first_srr_6char}"
echo "processing ${n_process_at_once} at once..."

while read -r i
do
	srr_6char=${i:0: 6}
	srr_or_err=${i:0: 3} # able to handle SRR or ERR!
	echo "mkdir ${i}" >> DL_and_process_${i}
	echo "cd ${i}" >> DL_and_process_${i}
	echo "wget ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByRun/sra/${srr_or_err}/${srr_6char}/${i}/${i}.sra" >> DL_and_process_${i}
	echo "cd .." >> DL_and_process_${i}
done < ${srr_list}

iteration=1
group=0
while read -r i
do
	echo $iteration
	echo $i
	echo "bash DL_and_process_${i} &" >> DL_and_process_group_"$group"
	# if $iteration is divisible by $n_process_at_once
	if [ $(($iteration % $n_process_at_once)) -eq 0 ]
	then
		echo wait added
		echo wait >> DL_and_process_group_"$group"
		let group=${group}+1
	fi
	let iteration=${iteration}+1
done < ${srr_list}

# Count number of lines in file
n_srrs=$(awk '{x++} END {print x}' $srr_list)
# if n_srrs not divisible by n_process_at_once,
#  then add a wait at the end to make up for my sloppy programming
if [ $(( $n_srrs % $n_process_at_once )) -ne 0 ]
then
	echo wait >> DL_and_process_group_"$group"
fi

for i in `ls DL_and_process_group_*`
do
	echo executing ${i} ...
	bash $i
done

# clean up after yo-self, and save download lines to a text file
echo "# download_fastqs_lpc.sh ${1} ${2} ${3}" >> ${downloadlinepath}

for i in `ls ./DL_and*`
do
	echo "#${i}" >> ${downloadlinepath}
	cat "${i}" >> ${downloadlinepath}
	rm $i
done
