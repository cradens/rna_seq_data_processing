#!/usr/bin/env bash

srr_list=${1} # path/name to/of file with list of SRRs or ERRs
n_process_at_once=${2} # how many fastqs to download and process at once? Recommended: 6
ended=${3} # paired or single
delete_untrimmed_fastqs=${4} # keep_untrimmed|delete_untrimmed 
downloadlinepath=${5} # where to save the download lines?

# make sure sample_list is set
if [ -z ${srr_list} ]
then
	echo "srr_list is unset .. please specify file with list of SRRs (arg 1)"
	exit 1
fi

if [ ! -f ${srr_list} ]
then
	echo "${srr_list} is not a valid file path to a list of SRRs..."
	exit 1
fi

# make sure n_process_at_once is set
if [ -z ${n_process_at_once} ]
then
	echo "n_process_at_once is unset .. please number of SRRs to process at once (arg 2)"
	exit 1
fi

# make sure ended is set
if [ -z ${ended} ]
then
	echo "ended is unset .. please specify if sequencing was 'paired' or 'single' -ended (arg 3)"
	exit 1
fi

# make sure ended is OK
if [[ ! ${ended} = "paired" ]]
then
        if [[ ! ${ended} = "single" ]]
        then
                echo "${ended} isn't paired or single ... (arg 3)"
                exit 1
        fi 
fi

# make sure delete_untrimmed_fastqs is set
if [ -z ${delete_untrimmed_fastqs} ]
then
        echo "delete_untrimmed_fastqs is unset .. please specify if you want to delete 'delete_untrimmed' or keep 'keep_untrimmed' fastqs after trimming... (arg 4)"
        exit 1
fi

# make sure delete_untrimmed_fastqs is OK
if [[ ! ${delete_untrimmed_fastqs} = "delete_untrimmed" ]]
then
        if [[ ! ${delete_untrimmed_fastqs} = "keep_untrimmed" ]]
        then
                echo "${delete_untrimmed_fastqs} isn't 'delete_untrimmed' or 'keep_untrimmed' ... (arg 4)"
                exit 1
        fi
fi


# make sure downloadlinepath is set
if [ -z ${downloadlinepath} ]
then
	echo "downloadlinepath is unset .. please say where you want the download lines saved to (arg 4)"
	exit 1
fi

if [ -f ${downloadlinepath} ]
then
	echo "${downloadlinepath} already exists, please delete, move, or change name ..."
	exit 1
fi

if [ -d ${downloadlinepath} ]
then
        echo "${downloadlinepath} is a directory, please give a path and name for a file to save the lines to..."
        exit 1
fi

# Check dependencies are installed
command -v fastq-dump >/dev/null 2>&1 || { echo >&2 "I require fastq-dump but it's not installed.  Aborting."; exit 1; }
command -v trim_galore >/dev/null 2>&1 || { echo >&2 "I require trim_galore but it's not installed.  Aborting."; exit 1; }
command -v fastqc >/dev/null 2>&1 || { echo >&2 "I require fastqc but it's not installed.  Aborting."; exit 1; }

#first_srr=$( head -1 ${srr_list} )
#first_srr_6char=${first_srr:0: 6}
#echo "downloading SRRs with the following prefix: ${first_srr_6char}"
echo "processing ${n_process_at_once} at once..."

# if data are single ended
if [[ ${ended} = "single" ]]
	then
	while read -r i
	do
		srr_6char=${i:0: 6}
		echo wget ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByRun/sra/SRR/${srr_6char}/${i}/${i}.sra > DL_and_process_${i}
		echo fastq-dump ./${i}.sra --split-3 >> DL_and_process_${i}
		echo rm ${i}.sra >> DL_and_process_${i}
		echo mkdir ${i} >> DL_and_process_${i}
		echo mv ${i}.fastq ./${i}/ >> DL_and_process_${i}
		echo cd ${i} >> DL_and_process_${i}
		echo fastqc ${i}.fastq -o ./ >> DL_and_process_${i}
		echo trim_galore -stringency 5 -length 35 -q 20 -o ./ ${i}.fastq >> DL_and_process_${i}
		if [[ ${delete_untrimmed_fastqs} = "delete_untrimmed" ]]
		then
			echo rm ${i}.fastq >> DL_and_process_${i}
		fi
		echo fastqc ${i}_trimmed.fq -o ./ >> DL_and_process_${i}
		echo cd .. >> DL_and_process_${i}
	done < ${srr_list}
else # else paired
	while read -r i
	do
		srr_6char=${i:0: 6}
		echo wget ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByRun/sra/SRR/${srr_6char}/${i}/${i}.sra > DL_and_process_${i}
		echo fastq-dump ./${i}.sra --split-3 >> DL_and_process_${i}
		echo rm ${i}.sra >> DL_and_process_${i}
		echo mkdir ${i} >> DL_and_process_${i}
		echo mv ${i}_1.fastq ./${i}_trimtest/ >> DL_and_process_${i}
		echo mv ${i}_2.fastq ./${i}_trimtest/ >> DL_and_process_${i}
		echo cd ${i} >> DL_and_process_${i}
		echo mkdir ./fastqc_1 >> DL_and_process_${i}
		echo mkdir ./fastqc_2 >> DL_and_process_${i}
		echo "fastqc "$i"_1.fastq -o ./fastqc_1 &" >> DL_and_process_"$i"
		echo fastqc "$i"_2.fastq -o ./fastqc_2 >> DL_and_process_"$i"
		echo wait >> DL_and_process_"$i"
		echo "trim_galore --paired -stringency 5 -length 35 -q 20 -o ./ "$i"_1.fastq "$i"_2.fastq" >> DL_and_process_"$i"
		if [[ ${delete_untrimmed_fastqs} = "delete_untrimmed" ]]
                then
                        echo rm ${i}_1.fastq >> DL_and_process_${i}
			echo rm ${i}_2.fastq >> DL_and_process_${i}
                fi
		echo mkdir ./fastqc_1_trimmed >> DL_and_process_"$i"
		echo mkdir ./fastqc_2_trimmed >> DL_and_process_"$i"
		echo "fastqc "$i"_1_val_1.fq -o ./fastqc_1_trimmed &" >> DL_and_process_"$i"
		echo fastqc "$i"_2_val_2.fq -o ./fastqc_2_trimmed >> DL_and_process_"$i"
		echo wait >> DL_and_process_"$i"
		echo cd .. >> DL_and_process_"$i"
	done < ${srr_list}
fi

iteration=1
group=0
while read -r i
do
	echo $iteration
	echo $i
	echo "bash DL_and_process_${i} &" >> DL_and_process_group_"$group"
	# if $iteration is divisible by $n_process_at_once
	if [ $(($iteration % $n_process_at_once)) -eq 0 ]
	then
		echo wait added
		echo wait >> DL_and_process_group_"$group"
		let group=${group}+1
	fi
	let iteration=${iteration}+1
done < ${srr_list}

# Count number of lines in file
n_srrs=$(awk '{x++} END {print x}' $srr_list)
# if n_srrs not divisible by n_process_at_once,
#  then add a wait at the end to make up for my sloppy programming
if [ $(( $n_srrs % $n_process_at_once )) -ne 0 ]
then
	echo wait >> DL_and_process_group_"$group"
fi

for i in `ls DL_and_process_group_*`
do
	echo executing ${i} ...
	bash $i
done

# clean up after yo-self, and save download lines to a text file
echo "# download_fastqs.sh ${1} ${2} ${3} ${4} ${5}" >> ${downloadlinepath}

for i in `ls ./DL_and*`
do
	echo "${i}" >> ${downloadlinepath}
	cat "${i}" >> ${downloadlinepath}
	rm $i
done

