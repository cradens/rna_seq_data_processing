#!/usr/bin/env bash

# Caleb Matthew Radens

sample_list=${1} # path/name to/of file with list of fastqs
# if paired, every 2 fastqs must be a pair
species=${2} # hg19 or mm10
ended=${3} # paired or single
length=${4} # read length
n_threads=${5} #number of threads to align with; 8-12 is sufficient probably
nchar_endwith=${6} # use this to indicate how many characters at the end of each
# sample are indicating the filetype and/or pair ID. For example, if you have donor_A_pair1.fq and donor_A_pair2.fq, 
# set nchar_endwith=9
delete_fastqs=${7} # keep_fastq|delete_fastq after aligning?
write_runlines_to=${7} # specify file where to save your run lines to...

# How to run:
# bash /data/STAR_DATA/align_fastqs.sh fastq_file_list.txt mm10_or_hg19 paired_or_single read_length number_of_threads nchar_endwith path_to_outlog


# EXAMPLE how to run for paired data;
# 1) save a file with a line-by-line list of fastqs. Each two lines correspond to a pair of fastqs
#       donor_A_1.fq
#       donor_A_2.fq
#       donor_B_1.fq
#       donor_B_2.fq
#       etc
# 2) If you data are from mouse, read length 100, run the following line:
# bash /data/STAR_DATA/align_fastqs.sh fastq_file_list.txt mm10 paired 100 8 5 /path/to/runlines/here.txt

# make sure sample_list is set
if [ -z ${sample_list} ]
then
        echo "sample_list is unset .. please specify file with list of fastqs (arg 1)"
        exit 1
fi

if [ ! -f ${sample_list} ]
then
	echo "${sample_list} isn't recognized as a valid file..."
	exit 1
fi

wcres=($(wc ${sample_list}))
n_samps=${wcres[0]} # 1st elem from wc is the # of lines...

# make sure species is OK
if [[ ! ${species} = "hg19" ]]
then
        if [[ ! ${species} = "mm10" ]]
        then
                echo "${species} isn't mm10 or hg19... (arg 2)"
                exit 1
        fi
fi

# make sure ended is OK
if [[ ! ${ended} = "paired" ]]
then
        if [[ ! ${ended} = "single" ]]
        then
                echo "${ended} isn't paired or single ... (arg 3)"
                exit 1
        fi
fi

# make sure read length is set
if [ -z ${length} ]
then
        echo "length is unset .. please specify read length (arg 4)"
        exit 1
fi

# make sure n_threads is set
if [ -z ${n_threads} ]
then
        echo "n_threads is unset .. please specify number of threads to use (arg 5)"
        exit 1
fi

# make sure nchar_endwith is set
if [ -z ${nchar_endwith} ]
then
        echo "nchar_endwith is not set... please specify number of char to cut off (arg 6)"
        exit 1
fi

# make sure delete_fastqs is set
if [ -z ${delete_fastqs} ]
then
        echo "delete_fastqs is unset .. please specify if you want to delete 'delete_fastqs' or keep 'keep_fastqs' fastqs after aligning... (arg 7)"
        exit 1
fi

# make sure delete_fastqs is OK
if [[ ! ${delete_fastqs} = "delete_fastqs" ]]
then
        if [[ ! ${delete_fastqs} = "keep_fastqs" ]]
        then
                echo "${delete_fastqs} isn't 'delete_fastqs' or 'keep_fastqs' ... (arg 7)"
                exit 1
        fi
fi

# make sure write_runlines_to is set
if [ -z ${write_runlines_to} ]
then
        echo "write_runlines_to is unset .. please specify file to write runlines to (arg 7)"
        exit 1
fi

# make sure write_runlines_to is set to a file that doesn't already exist
if [ -f ${write_runlines_to} ]
then
        echo "${write_runlines_to} already exists please delete, move, or change name ... "
        exit 1
fi

# make sure write_runlines_to is set to a file that doesn't already exist
if [ -d ${write_runlines_to} ]
then
        echo "${write_runlines_to} is a folder, please specify filename, too "
        exit 1
fi

# Check dependences are installed
command -v STAR >/dev/null 2>&1 || { echo >&2 "I require STAR but it's not installed.  Aborting."; exit 1; }
command -v samtools >/dev/null 2>&1 || { echo >&2 "I require samtools but it's not installed.  Aborting."; exit 1; }

printf "generating lines to STAR align ${n_samps} ${ended}-end sequenced samples (using ${n_threads} threads)\nSpecies: ${species}\nSequence length: ${length}\n"

lines_run="#bash /data/STAR_DATA/align_fastqs.sh ${1} ${2} ${3} ${4} ${5} ${6} ${7}\n"

# make sure genome was generated for given species+length

# if genome wasn't generated for given species/length
# Alex Dobin discuss this here:
# https://groups.google.com/forum/#!msg/rna-star/h9oh10UlvhI/BfSPGivUHmsJ
# TLDR; you can use the len100 genomes, and in most cases, that will be fine.
if [ ! -d "/data/STAR_DATA/${species}_len${length}" ]
then
        if [[ ${species} = "hg19" ]]
        then
                gtfpath="/data/STAR_DATA/Homo_sapiens_GRCh37_75_GTF/Homo_sapiens_GRCh37_75_chrFIX.gtf"
        else
                gtfpath="/data/STAR_DATA/mm10_gtf/Mus_musculus.GRCm38.82_chrFIX.gtf"
        fi
        printf "/data/STAR_DATA/${species}_len${length} doesn't exist yet... please consider generating a new genome index with following commands:\n\n"
        echo "cd /data/STAR_DATA/; bash gen_STAR_${species}_genome_specify_len_and_gtf.bash ${length} ${gtfpath}; cd -"
        exit 1
fi

# load genome into memory (so you don't have to re-load it in each loop...
#STAR --genomeLoad LoadAndExit --genomeDir /data/STAR_DATA/${species}_len${length}
loadgenomeline="STAR --genomeLoad LoadAndExit --genomeDir /data/STAR_DATA/${species}_len${length}"
lines_run="${lines_run}${loadgenomeline}\n"

# if data are paird
if [[ ${ended} = "paired" ]]
        then
        iteration=1
        fastqpair=""
        #for fastq in `cat $sample_list`
        while read -r fastq
	do
                # quote all weird characters in filename i.e. spaces
		fastq=$(printf %q "$fastq")
		# save first fastq
                if [ ! $(($iteration % 2)) -eq 0 ]
                then
                        fastqpair="${fastq}"
                # if $iteration is divisible by 2, then we're seing the fist fastq's mate pair
                elif [ $(($iteration % 2)) -eq 0 ]
                then
                        out_name="${fastq:0:-${nchar_endwith}}"

                        fastqpair="${fastqpair} ${fastq}"
                        # note: {fastq:0:-5} will use all the characters except the last 5. For example, if your paired fastqs are named like this:
                        # donor_A_1.fq
                        # donor_A_2.fq
                        # the outFileNamePrefix will look like:
                        # ./donor_A.
#                        echo "Aligning ${fastqpair} ..."
			echoline="echo Aligning ${fastqpair} ...\n"
			lines_run="${lines_run}${echoline}"

#                        STAR --genomeLoad LoadAndKeep --limitBAMsortRAM 10000000000 --genomeDir /data/STAR_DATA/${species}_len${length} --readFilesIn ${fastqpair} --runThreadN ${n_threads} --outSAMtype BAM SortedByCoordinate --outFileNamePrefix ${out_name}. --outSAMattributes All --alignSJoverhangMin 8
                        starline="STAR --genomeLoad LoadAndKeep --limitBAMsortRAM 10000000000 --genomeDir /data/STAR_DATA/${species}_len${length} --readFilesIn ${fastqpair} --runThreadN ${n_threads} --outSAMtype BAM SortedByCoordinate --outFileNamePrefix ${out_name}. --outSAMattributes All --alignSJoverhangMin 8"
                        lines_run="${lines_run}${starline}\n"

			if [[ ${delete_fastqs} = "delete_fastqs" ]]
			then
				rm_line="rm ${fastqpair}"
				lines_run="${lines_run}${rm_line}\n"
			fi

#                        mv ${out_name}.Aligned.sortedByCoord.out.bam ${out_name}.bam
                        renamebamline="mv ${out_name}.Aligned.sortedByCoord.out.bam ${out_name}.bam"
                        lines_run="${lines_run}${renamebamline}\n"

#                        echo "Indexing ${out_name}.bam ..."
			echoline="echo Indexing ${out_name}.bam ..."
			lines_run="${lines_run}${echoline}\n"

#                        samtools index ${out_name}.bam
                        indexline="samtools index ${out_name}.bam"
                        lines_run="${lines_run}${indexline}\n"

                fi
                let iteration=${iteration}+1
        done < ${sample_list}
else # else data are single-ended
        #for fastq in `cat $sample_list`
        while read -r fastq
	do
                # quote all weird characters in filename i.e. spaces
                fastq=$(printf %q "$fastq")
		out_name="${fastq:0:-${nchar_endwith}}"

#                echo "Aligning ${fastq} ..."
		echoline="echo Aligning ${fastq} ..."
		lines_run="${lines_run}${echoline}\n"	

#                STAR --genomeLoad LoadAndKeep --limitBAMsortRAM 10000000000 --genomeDir /data/STAR_DATA/${species}_len${length} --readFilesIn ${fastq} --runThreadN ${n_threads} --outSAMtype BAM SortedByCoordinate --outFileNamePrefix ${out_name}. --outSAMattributes All --alignSJoverhangMin 8
                starline="STAR --genomeLoad LoadAndKeep --limitBAMsortRAM 10000000000 --genomeDir /data/STAR_DATA/${species}_len${length} --readFilesIn ${fastq} --runThreadN ${n_threads} --outSAMtype BAM SortedByCoordinate --outFileNamePrefix ${out_name}. --outSAMattributes All --alignSJoverhangMin 8"
                lines_run="${lines_run}${starline}\n"

		if [[ ${delete_fastqs} = "delete_fastqs" ]]
		then
			rm_line="rm ${fastq}"
			lines_run="${lines_run}${rm_line}\n"
		fi

#                mv ${out_name}.Aligned.sortedByCoord.out.bam ${out_name}.bam
                renamebamline="mv ${out_name}.Aligned.sortedByCoord.out.bam ${out_name}.bam"
                lines_run="${lines_run}${renamebamline}\n"

#                echo "Indexing ${out_name}.bam ..."
		echoline="echo Indexing ${out_name}.bam ..."
		lines_run="${lines_run}${echoline}\n"
	
#                samtools index ${out_name}.bam
                indexline="samtools index ${out_name}.bam"
                lines_run="${lines_run}${indexline}\n"

        done < ${sample_list}
fi

# remove the loaded genome from memory...
#STAR --genomeLoad Remove --genomeDir /data/STAR_DATA/${species}_len${length}
removegenomeline="STAR --genomeLoad Remove --genomeDir /data/STAR_DATA/${species}_len${length}"
lines_run="${lines_run}${removegenomeline}\n"

rm_extraneousfiles="rm -r _STARtmp\nrm Log.progress.out\nrm Aligned.out.sam"
lines_run="${lines_run}${rm_extraneousfiles}\n"

printf "Finished generating STAR aligning lines! Saving the command lines used to generate the BAMs to this file:\n"
echo "${write_runlines_to}"
printf "${lines_run}" > ${write_runlines_to}

