#!/usr/bin/env bash

# adapted from matthew gazzara

# This script checks for reads from GAPDH in mouse and human

bamfile=${1} # bam file to check
chr=${2} # either leave blank or chr

samtools=/project/klynclab/software/samtools-1.9/samtools

####PE read check####
###mm10
echo "Check for reverse-stranded and paired reads from mm10:"
${samtools} view -f 96 ${bamfile} ${chr}6:125161852-125166467 | wc -l #first in pair, mate reverse
${samtools} view -f 144 ${bamfile} ${chr}6:125161852-125166467 | wc -l #sec in pair, read reverse
echo "Check for forward-stranded and paired reads from mm10:"
${samtools} view -f 80 ${bamfile} ${chr}6:125161852-12516646 | wc -l # first mate in pair, read reverse
${samtools} view -f 160 ${bamfile} ${chr}6:125161852-125166467 | wc -l #sec in pair, mate reverse
###hg19
echo "Check for reverse-stranded and paired reads from hg19:"
${samtools} view -f 96 ${bamfile} ${chr}12:6643585-6647537 | wc -l #first in pair, mate reverse
${samtools} view -f 144 ${bamfile} ${chr}12:6643585-6647537 | wc -l #sec in pair, read reverse
echo "Check for forward-stranded and paired reads from hg19:"
${samtools} view -f 80 ${bamfile} ${chr}12:6643585-6647537 | wc -l #first in pair, read reverse
${samtools} view -f 160 ${bamfile} ${chr}12:6643585-6647537 | wc -l #sec in pair, mate reverse
###hg38
echo "Check for reverse-stranded and paired reads from hg38:"
${samtools} view -f 96 ${bamfile} ${chr}12:6533927-6538371 | wc -l #first in pair, mate reverse
${samtools} view -f 144 ${bamfile} ${chr}12:6533927-6538371 | wc -l #sec in pair, read reverse
echo "Check for forward-stranded and paired reads from hg38:"
${samtools} view -f 80 ${bamfile} ${chr}12:6533927-6538371 | wc -l #first in pair, read reverse
${samtools} view -f 160 ${bamfile} ${chr}12:6533927-6538371 | wc -l #sec in pair, mate reverse

####SE read check####
###mm10
echo "Check if single-ended reads mm10:"
${samtools} view -F 16 ${bamfile} ${chr}6:125161852-125166467 | wc -l #read forward strand
${samtools} view -f 16 ${bamfile} ${chr}6:125161852-125166467 | wc -l #read reverse strand
###hg19
echo "Check if single-ended reads hg19:"
${samtools} view -F 16 ${bamfile} ${chr}12:6643585-6647537 | wc -l #read forward strand
${samtools} view -f 16 ${bamfile} ${chr}12:6643585-6647537 | wc -l #read reverse strand
###hg38
echo "Check if single-ended reads hg38:"
${samtools} view -F 16 ${bamfile} ${chr}12:6533927-6538371 | wc -l #read forward strand
${samtools} view -f 16 ${bamfile} ${chr}12:6533927-6538371 | wc -l #read reverse strand

